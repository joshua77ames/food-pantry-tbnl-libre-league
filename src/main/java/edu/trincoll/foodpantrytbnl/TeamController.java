package edu.trincoll.foodpantrytbnl;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TeamController {
    private final List<String> teamMembers =
            List.of("Myri", "Hamim", "Cordelia");


    @GetMapping("/team")  // localhost:8080/team
    public List<String> getTeamMembers() {
        return teamMembers;
    }
}
